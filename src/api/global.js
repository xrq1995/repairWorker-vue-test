export function apiBase() {

    let hostName = window.location.hostname,
        API_BASE_URL = 'http://172.17.100.193:8088/repair';
    if (hostName === 'localhost') {
        API_BASE_URL = 'http://localhost:8090';
    } else {
        API_BASE_URL = 'http://172.17.100.193:8088/repair';
    }
    // else if (hostName === '101.76.160.197') {
    //     API_BASE_URL = 'http://101.76.160.197:8080'
    // }

    return API_BASE_URL;
}

/**
 * 公共API接口
 * @constructor
 */
export function COMMON_API() {

}